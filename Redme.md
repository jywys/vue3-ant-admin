vs_code 需要添加配置

// Enable vue/script-setup-uses-vars rule
  "vetur.validation.template": false,

演示地址： http://42.192.158.13/

此项目 后端 springboot 2.6.1 + sa-token-redis + mybatis-plus + mysql + mongodb
前端 vue3.0 + js/ts + ant-design-vue2.2.8 + axios + scss

// 异步等待多个接口结果
const aa = async () => {
// 等待 ccc 执行完成打印结果
const bb = await ccc();
console.log(bb)
}
