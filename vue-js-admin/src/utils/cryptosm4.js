export const pwdKey = '1234567890123456'
export const ivKey = '1234567890123456'
import GmCrypt from 'gm-crypt'

const SM4 = GmCrypt.sm4

export function encrypt(pwd) {
    const sm4Config = {
        key: pwdKey,
        mode: 'cbc',
        iv: ivKey,
        cipherType: 'base64'
    }
    const sm4Util = new SM4(sm4Config)
    return sm4Util.encrypt(pwd, pwdKey)
}

export function decrypt(pwd) {
    const sm4Config = {
        key: pwdKey,
        mode: 'cbc',
        iv: ivKey,
        cipherType: 'base64'
    }
    const sm4Util = new SM4(sm4Config)
    const plaintext = sm4Util.decrypt(pwd, pwdKey)
    return plaintext;
}