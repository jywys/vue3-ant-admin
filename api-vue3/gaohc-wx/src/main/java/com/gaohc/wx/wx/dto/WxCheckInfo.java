package com.gaohc.wx.wx.dto;

import lombok.Data;

@Data
public class WxCheckInfo {
    private String signature;
    private String timestamp;
    private String nonce;
    private String echostr;
}
