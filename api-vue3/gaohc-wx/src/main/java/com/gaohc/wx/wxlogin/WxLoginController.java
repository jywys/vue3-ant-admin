package com.gaohc.wx.wxlogin;

import com.gaohc.common.core.controller.BaseController;
import com.gaohc.common.core.domain.AjaxJson;
import com.gaohc.wx.wxlogin.domain.WxUserInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author 成大事
 * @since 2022/7/27 22:44
 */
@Slf4j
@RestController
@RequestMapping("/wx/user")
@RequiredArgsConstructor
public class WxLoginController extends BaseController {

    private final UserInfoService userInfoService;

    /**
     * 登陆接口
     */
    @GetMapping("/login")
    public AjaxJson login(@RequestParam("code") String code) {
        return userInfoService.login(code);
    }

    /**
     * <pre>
     * 获取用户信息接口
     * </pre>
     */
    @PostMapping("/getUserInfo")
    public AjaxJson getUserInfo(@RequestBody WxUserInfo userInfo) {
        return userInfoService.getUserInfo(userInfo);
    }
}
