package com.gaohc.wx.oss;

import com.gaohc.common.core.controller.BaseController;
import com.gaohc.common.core.domain.AjaxJson;
import com.qiniu.util.Auth;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/wx/oss")
public class QnController extends BaseController {

    @Value("${qiniu.access}")
    private String access;
    @Value("${qiniu.secret}")
    private String secret;
    @Value("${qiniu.name}")
    private String name;

    @GetMapping("getToken")
    public AjaxJson getUploadToken (String key) {
        Auth auth = Auth.create(access, secret);
        String upToken = auth.uploadToken(name, key);
        return AjaxJson.getSuccessData(upToken);
    }


}
