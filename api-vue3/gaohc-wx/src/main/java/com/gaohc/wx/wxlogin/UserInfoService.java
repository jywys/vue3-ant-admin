package com.gaohc.wx.wxlogin;

import com.gaohc.common.core.domain.AjaxJson;
import com.gaohc.wx.wxlogin.domain.WxUserInfo;

/**
 * @author 成大事
 * @since 2022/7/27 22:47
 */
public interface UserInfoService {

    /**
     * 登录
     * @param code code
     * @return   WxMaJscode2SessionResult
     */
    AjaxJson login(String code);

    /**
     * 获取用户信息
     * @param userInfo  包含一些加密的信息
     * @return  WxMaUserInfo
     */
    AjaxJson getUserInfo(WxUserInfo userInfo);
}

