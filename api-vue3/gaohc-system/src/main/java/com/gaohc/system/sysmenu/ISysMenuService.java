package com.gaohc.system.sysmenu;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gaohc.common.core.domain.TreeSelect;
import com.gaohc.common.core.domain.entity.SysMenu;
import com.gaohc.system.vo.RouterVo;

import java.util.List;
import java.util.Set;

/**
 * 菜单 业务层
 * 
 * @author ruoyi
 */
public interface ISysMenuService extends IService<SysMenu>
{



    /**
     * 根据用户ID查询权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    public Set<String> selectMenuPermsByUserId(Long userId);


    /**
     * 构建前端路由所需要的菜单
     * 
     * @param menus 菜单列表
     * @return 路由列表
     */
    public List<RouterVo> buildMenus(List<SysMenu> menus);


    List<SysMenu> selectMenuTreeByUserId(Long userId);

    List<SysMenu> selectMenuList(Long loginId);

    List<TreeSelect> buildMenuTreeSelect(List<SysMenu> menus);

    List<Long> selectMenuListByRoleId(Long roleId);

    String checkMenuNameUnique(SysMenu menu);

    boolean hasChildByMenuId(Long menuId);

}
