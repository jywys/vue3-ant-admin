package com.gaohc.system.sysoperlog;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gaohc.common.annotation.Log;
import com.gaohc.common.core.domain.AjaxJson;
import com.gaohc.common.enums.BusinessType;
import com.gaohc.common.utils.poi.ExcelUtil;
import com.gaohc.system.sysoperlog.SysOperLog;
import com.gaohc.system.sysoperlog.ISysOperLogService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 操作日志记录
 * 
 * @author ruoyi
 */
@SaCheckLogin
@RestController
@RequestMapping("/monitor/operlog")
public class SysOperlogController
{
    @Resource
    private ISysOperLogService operLogService;

    @SaCheckPermission("monitor:operlog:list")
    @GetMapping("/list")
    public AjaxJson list(SysOperLog operLog)
    {
        Page<SysOperLog> page = new Page<>(1, 10);
        operLogService.lambdaQuery().page(page);
        return AjaxJson.getSuccessData(page);
    }

    @Log(title = "操作日志", businessType = BusinessType.EXPORT)
    @SaCheckPermission("monitor:operlog:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysOperLog operLog)
    {
        List<SysOperLog> list = operLogService.selectOperLogList(operLog);
        ExcelUtil<SysOperLog> util = new ExcelUtil<SysOperLog>(SysOperLog.class);
        util.exportExcel(response, list, "操作日志");
    }

    @Log(title = "操作日志", businessType = BusinessType.DELETE)
    @SaCheckPermission("monitor:operlog:remove")
    @DeleteMapping("/{operIds}")
    public AjaxJson remove(@PathVariable Long[] operIds)
    {
        operLogService.deleteOperLogByIds(operIds);
        return AjaxJson.getSuccess();
    }

    @Log(title = "操作日志", businessType = BusinessType.CLEAN)
    @SaCheckPermission("monitor:operlog:remove")
    @DeleteMapping("/clean")
    public AjaxJson clean()
    {
        operLogService.cleanOperLog();
        return AjaxJson.getSuccess();
    }
}
