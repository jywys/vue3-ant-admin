package com.gaohc.system.sysuser;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gaohc.system.sysuser.domain.SysRoleMenu;

/**
 * Package:com.gaohc.system.service
 * Description:
 *
 * @date:2021/12/16 17:28
 * @author:gaohuichao
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {
    boolean checkMenuExistRole(Long menuId);
}
