package com.gaohc.system.sysuser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gaohc.common.core.domain.entity.SysRole;

import java.util.List;

/**
 * Package:com.gaohc.system.mapper
 * Description:
 *
 * @date:2021/12/16 16:11
 * @author:gaohuichao
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {
    List<Long> selectRoleListByUserId(Long userId);

    List<SysRole> selectRolePermissionByUserId(Long userId);

    List<SysRole> selectRolesByUserId(Long userId);
}
