package com.gaohc.system.sysoperlog.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gaohc.system.sysoperlog.SysOperLog;

public interface SysOperLogMapper extends BaseMapper<SysOperLog> {
}
