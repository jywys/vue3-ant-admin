package com.gaohc.system.sysdict;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gaohc.common.core.domain.entity.SysDictData;
import com.gaohc.common.core.domain.entity.SysDictType;

import java.util.List;

/**
 * 字典 业务层
 * 
 * @author ruoyi
 */
public interface ISysDictTypeService extends IService<SysDictType> {
    /**
     * 根据字典类型查询字典数据
     * 
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    public List<SysDictData> selectDictDataByType(String dictType);

    String checkDictTypeUnique(SysDictType dict);

    void resetDictCache();

}
