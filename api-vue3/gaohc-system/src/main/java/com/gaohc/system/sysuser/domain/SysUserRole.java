package com.gaohc.system.sysuser.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户和角色关联 sys_user_role
 * 
 * @author ruoyi
 */
@Data
public class SysUserRole
{
    /** 用户ID */
    @TableId
    private Long userId;
    
    /** 角色ID */
    private Long roleId;

}
