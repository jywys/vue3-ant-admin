package com.gaohc.system.sysdict;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gaohc.common.annotation.Log;
import com.gaohc.common.constant.UserConstants;
import com.gaohc.common.core.domain.AjaxJson;
import com.gaohc.common.core.domain.entity.SysDictType;
import com.gaohc.common.enums.BusinessType;
import com.gaohc.common.utils.poi.ExcelUtil;
import com.gaohc.system.sysdict.ISysDictTypeService;
import com.gaohc.system.sysuser.ISysUserService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * 数据字典信息
 * 
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/dict/type")
public class SysDictTypeController
{
    @Resource
    private ISysDictTypeService dictTypeService;
    @Resource
    private ISysUserService sysUserService;


    @SaCheckPermission("system:dict:list")
    @GetMapping("/list")
    public AjaxJson list(SysDictType dictType)
    {
        Page<SysDictType> page = new Page<>(1, 10);
        dictTypeService.lambdaQuery().page(page);
        return AjaxJson.getSuccessData(page);
    }

    @Log(title = "字典类型", businessType = BusinessType.EXPORT)
    @SaCheckPermission("system:dict:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response)
    {
        List<SysDictType> list = dictTypeService.list();
        ExcelUtil<SysDictType> util = new ExcelUtil<>(SysDictType.class);
        util.exportExcel(response, list, "字典类型");
    }

    /**
     * 查询字典类型详细
     */
    @SaCheckPermission("system:dict:query")
    @GetMapping(value = "/{dictId}")
    public AjaxJson getInfo(@PathVariable Long dictId)
    {
        return AjaxJson.getSuccessData(dictTypeService.getById(dictId));
    }

    /**
     * 新增字典类型
     */
    @SaCheckPermission("system:dict:add")
    @Log(title = "字典类型", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxJson add(@Validated @RequestBody SysDictType dict)
    {
        if (UserConstants.NOT_UNIQUE.equals(dictTypeService.checkDictTypeUnique(dict)))
        {
            return AjaxJson.getError("新增字典'" + dict.getDictName() + "'失败，字典类型已存在");
        }
        dictTypeService.save(dict);
        return AjaxJson.getSuccess("添加成功");
    }

    /**
     * 修改字典类型
     */
    @SaCheckPermission("system:dict:edit")
    @Log(title = "字典类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxJson edit(@Validated @RequestBody SysDictType dict)
    {
        if (UserConstants.NOT_UNIQUE.equals(dictTypeService.checkDictTypeUnique(dict)))
        {
            return AjaxJson.getError("修改字典'" + dict.getDictName() + "'失败，字典类型已存在");
        }
        dictTypeService.updateById(dict);
        return AjaxJson.getSuccess("修改成功");
    }

    /**
     * 删除字典类型
     */
    @SaCheckPermission("system:dict:remove")
    @Log(title = "字典类型", businessType = BusinessType.DELETE)
    @DeleteMapping("/{dictIds}")
    public AjaxJson remove(@PathVariable Long[] dictIds)
    {
        dictTypeService.removeByIds(Arrays.asList(dictIds));
        return AjaxJson.getSuccess("删除成功");
    }

    /**
     * 刷新字典缓存
     */
    @SaCheckPermission("system:dict:remove")
    @Log(title = "字典类型", businessType = BusinessType.CLEAN)
    @DeleteMapping("/refreshCache")
    public AjaxJson refreshCache()
    {
        dictTypeService.resetDictCache();
        return AjaxJson.getSuccess();
    }

    /**
     * 获取字典选择框列表
     */
    @GetMapping("/optionselect")
    public AjaxJson optionselect()
    {
        List<SysDictType> dictTypes = dictTypeService.list();
        return AjaxJson.getSuccessData(dictTypes);
    }
}
