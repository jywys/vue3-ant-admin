package com.gaohc.system.sysuser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gaohc.system.sysuser.domain.SysUserRole;

/**
 * Package:com.gaohc.system.mapper
 * Description:
 *
 * @date:2021/12/16 17:14
 * @author:gaohuichao
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
}
