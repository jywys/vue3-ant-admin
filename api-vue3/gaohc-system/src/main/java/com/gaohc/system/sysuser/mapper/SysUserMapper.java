package com.gaohc.system.sysuser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gaohc.common.core.domain.entity.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {
}
