package com.gaohc.system.sysmenu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gaohc.common.core.domain.entity.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Package:com.gaohc.system.mapper
 * Description:
 *
 * @date:2021/12/16 16:12
 * @author:gaohuichao
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> selectMenuTreeAll();
    List<SysMenu> selectMenuListByUserId(Long userId);

    List<String> selectMenuPermsByUserId(Long userId);

    List<Long> selectMenuListByRoleId(@Param("roleId") Long roleId, @Param("menuCheckStrictly") boolean menuCheckStrictly);
}
