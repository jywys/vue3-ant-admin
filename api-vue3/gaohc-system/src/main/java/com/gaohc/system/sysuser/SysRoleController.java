package com.gaohc.system.sysuser;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gaohc.common.annotation.Log;
import com.gaohc.common.constant.UserConstants;
import com.gaohc.common.core.controller.BaseController;
import com.gaohc.common.core.domain.AjaxJson;
import com.gaohc.common.core.domain.entity.SysRole;
import com.gaohc.common.core.domain.entity.SysUser;
import com.gaohc.common.enums.BusinessType;
import com.gaohc.common.utils.poi.ExcelUtil;
import com.gaohc.system.sysuser.ISysRoleService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * 角色信息
 * 
 * @author ruoyi
 */
@SaCheckLogin
@RestController
@RequestMapping("/system/role")
public class SysRoleController extends BaseController
{
    @Resource
    private ISysRoleService roleService;


    @SaCheckPermission("system:role:list")
    @GetMapping("/list")
    public AjaxJson list()
    {

        Page<SysRole> page = new Page<>(1, 10);
        roleService.page(page);
        return AjaxJson.getSuccessData(page);
    }

    @Log(title = "角色管理", businessType = BusinessType.EXPORT)
    @SaCheckPermission("system:role:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysRole role)
    {
        List<SysRole> list = roleService.selectRoleList(role);
        ExcelUtil<SysRole> util = new ExcelUtil<>(SysRole.class);
        util.exportExcel(response, list, "角色数据");
    }
    /**
     * 根据角色编号获取详细信息
     */
    @SaCheckPermission("system:role:query")
    @GetMapping(value = "/{roleId}")
    public AjaxJson getInfo(@PathVariable Long roleId)
    {
        return AjaxJson.getSuccessData(roleService.getById(roleId));
    }


    /**
     * 查询已分配用户角色列表
     */
    @SaCheckPermission("system:role:list")
    @GetMapping("/authUser/allocatedList")
    public AjaxJson allocatedList(SysUser user)
    {
        Page<SysRole> page = new Page<>(1,10);

        return AjaxJson.getSuccessData(page);
    }

    /**
     * 新增角色
     */
    @SaCheckPermission("system:role:add")
    @Log(title = "角色管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxJson add(@Validated @RequestBody SysRole role)
    {
        if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role)))
        {
            return AjaxJson.getError("新增角色'" + role.getRoleName() + "'失败，角色名称已存在");
        }
        else if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role)))
        {
            return AjaxJson.getError("新增角色'" + role.getRoleName() + "'失败，角色权限已存在");
        }
        return AjaxJson.getSuccessData(roleService.save(role));

    }

    /**
     * 修改保存角色
     */
    @SaCheckPermission("system:role:edit")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxJson edit(@Validated @RequestBody SysRole role)
    {
        Long loginId = Long.parseLong(StpUtil.getLoginId().toString());

        if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role)))
        {
            return AjaxJson.getError("修改角色'" + role.getRoleName() + "'失败，角色名称已存在");
        }
        else if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role)))
        {
            return AjaxJson.getError("修改角色'" + role.getRoleName() + "'失败，角色权限已存在");
        }
        roleService.updateRoleById(role, getUserId());
        return AjaxJson.getSuccess();
    }

    /**
     * 状态修改
     */
    @SaCheckPermission("system:role:edit")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxJson changeStatus(@RequestBody SysRole role)
    {
        roleService.updateById(role);
        return AjaxJson.getSuccess("状态修改成功");
    }

    /**
     * 删除角色
     */
    @SaCheckPermission("system:role:remove")
    @Log(title = "角色管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{roleIds}")
    public AjaxJson remove(@PathVariable Long[] roleIds)
    {
        roleService.removeByIds(Arrays.asList(roleIds));
        return AjaxJson.getSuccess();
    }

    /**
     * 获取角色选择框列表
     */
    @SaCheckPermission("system:role:query")
    @GetMapping("/optionselect")
    public AjaxJson optionselect()
    {
        return AjaxJson.getSuccessData(roleService.selectRoleAll());
    }

}
