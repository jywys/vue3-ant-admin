package com.gaohc.system.sysdict.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gaohc.common.core.domain.entity.SysDictType;

public interface SysDictTypeMapper extends BaseMapper<SysDictType> {
}
