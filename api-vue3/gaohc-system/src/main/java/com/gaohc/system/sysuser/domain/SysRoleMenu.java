package com.gaohc.system.sysuser.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * 角色和菜单关联 sys_role_menu
 * 
 * @author ruoyi
 */
@Data
public class SysRoleMenu
{
    /** 角色ID */
    @TableId
    private Long roleId;
    
    /** 菜单ID */
    private Long menuId;

}
