package com.gaohc.system.sysconfig;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gaohc.common.annotation.Log;
import com.gaohc.common.constant.UserConstants;
import com.gaohc.common.core.domain.AjaxJson;
import com.gaohc.common.enums.BusinessType;
import com.gaohc.common.utils.poi.ExcelUtil;
import com.gaohc.system.sysconfig.domain.SysConfig;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 参数配置 信息操作处理
 * 
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/config")
public class SysConfigController
{
    @Resource
    private ISysConfigService configService;
    /**
     * 获取参数配置列表
     */
    @SaCheckPermission("system:config:list")
    @GetMapping("/list")
    public AjaxJson list(SysConfig config)
    {
        Page<SysConfig> page = new Page<>(1, 10);
        configService.lambdaQuery().page(page);
        return AjaxJson.getSuccessData(page);
    }

    @Log(title = "参数管理", businessType = BusinessType.EXPORT)
    @SaCheckPermission("system:config:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysConfig config)
    {
        List<SysConfig> list = configService.selectConfigList(config);
        ExcelUtil<SysConfig> util = new ExcelUtil<>(SysConfig.class);
        util.exportExcel(response, list, "参数数据");
    }

    /**
     * 根据参数编号获取详细信息
     */
    @SaCheckPermission("system:config:query")
    @GetMapping(value = "/{configId}")
    public AjaxJson getInfo(@PathVariable Long configId)
    {
        return AjaxJson.getSuccessData(configService.selectConfigById(configId));
    }

    /**
     * 根据参数键名查询参数值
     */
    @GetMapping(value = "/configKey/{configKey}")
    public AjaxJson getConfigKey(@PathVariable String configKey)
    {
        return AjaxJson.getSuccessData(configService.selectConfigByKey(configKey));
    }

    /**
     * 新增参数配置
     */
    @SaCheckPermission("system:config:add")
    @Log(title = "参数管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxJson add(@Validated @RequestBody SysConfig config)
    {
        if (UserConstants.NOT_UNIQUE.equals(configService.checkConfigKeyUnique(config)))
        {
            return AjaxJson.getError("新增参数'" + config.getConfigName() + "'失败，参数键名已存在");
        }
        return AjaxJson.getSuccessData(configService.insertConfig(config));
    }

    /**
     * 修改参数配置
     */
    @SaCheckPermission("system:config:edit")
    @Log(title = "参数管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxJson edit(@Validated @RequestBody SysConfig config)
    {
        if (UserConstants.NOT_UNIQUE.equals(configService.checkConfigKeyUnique(config)))
        {
            return AjaxJson.getError("修改参数'" + config.getConfigName() + "'失败，参数键名已存在");
        }
        return AjaxJson.getSuccessData(configService.updateConfig(config));
    }

    /**
     * 删除参数配置
     */
    @SaCheckPermission("system:config:remove")
    @Log(title = "参数管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{configIds}")
    public AjaxJson remove(@PathVariable Long[] configIds)
    {
        configService.deleteConfigByIds(configIds);
        return AjaxJson.getSuccess();
    }

    /**
     * 刷新参数缓存
     */
    @SaCheckPermission("system:config:remove")
    @Log(title = "参数管理", businessType = BusinessType.CLEAN)
    @DeleteMapping("/refreshCache")
    public AjaxJson refreshCache()
    {
        configService.resetConfigCache();
        return AjaxJson.getSuccess();
    }
}
