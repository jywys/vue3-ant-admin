package com.gaohc.system.sysdict.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gaohc.common.core.domain.entity.SysDictData;
import com.gaohc.common.utils.DictUtils;
import com.gaohc.system.sysdict.mapper.SysDictDataMapper;
import com.gaohc.system.sysdict.ISysDictDataService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典 业务层处理
 * 
 * @author ruoyi
 */
@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictData> implements ISysDictDataService {

    /**
     * 根据条件分页查询字典数据
     * 
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    @Override
    public Page<SysDictData> selectDictDataList(SysDictData dictData) {
        Page<SysDictData> page = new Page<>(1, 10);
        this.page(page);
        return page;
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     * 
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    @Override
    public String selectDictLabel(String dictType, String dictValue)
    {
        return this.lambdaQuery().eq(SysDictData::getDictType, dictType).eq(SysDictData::getDictValue, dictValue).one().getDictLabel();
    }

    /**
     * 根据字典数据ID查询信息
     * 
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    @Override
    public SysDictData selectDictDataById(Long dictCode) {
        return this.lambdaQuery().eq(SysDictData::getDictCode, dictCode).one();
    }

    /**
     * 批量删除字典数据信息
     * 
     * @param dictCodes 需要删除的字典数据ID
     */
    @Override
    public void deleteDictDataByIds(Long[] dictCodes) {
        for (Long dictCode : dictCodes)
        {
            SysDictData data = selectDictDataById(dictCode);
            this.removeById(dictCode);
            List<SysDictData> dictDatas = getListByDictType(data.getDictType());
            DictUtils.setDictCache(data.getDictType(), dictDatas);
        }
    }

    /**
     * 新增保存字典数据信息
     * 
     * @param data 字典数据信息
     * @return 结果
     */
    @Override
    public boolean insertDictData(SysDictData data)
    {
        boolean row = this.save(data);
        if (row)
        {
            List<SysDictData> dictDatas = getListByDictType(data.getDictType());
            DictUtils.setDictCache(data.getDictType(), dictDatas);
        }
        return row;
    }

    /**
     * 修改保存字典数据信息
     * 
     * @param data 字典数据信息
     * @return 结果
     */
    @Override
    public boolean updateDictData(SysDictData data)
    {
        boolean row = this.updateById(data);
        if (row)
        {
            List<SysDictData> dictDatas = getListByDictType(data.getDictType());
            DictUtils.setDictCache(data.getDictType(), dictDatas);
        }
        return row;
    }

    private List<SysDictData> getListByDictType(String dictType) {
        return this.lambdaQuery()
                .eq(SysDictData::getStatus, 0)
                .eq(SysDictData::getDictType,dictType)
                .orderByAsc(SysDictData::getDictValue).list();
    }
}
