package com.gaohc.system.sysuser;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gaohc.common.core.domain.entity.SysRole;
import com.gaohc.system.sysuser.domain.SysUserRole;

import java.util.List;

/**
 * Package:com.gaohc.system.service
 * Description:
 *
 * @date:2021/12/16 17:17
 * @author:gaohuichao
 */
public interface ISysUserRoleService extends IService<SysUserRole> {
    List<SysRole> selectRolesByUserId(Long userId);
}
