package com.gaohc.system.sysconfig.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gaohc.system.sysconfig.domain.SysConfig;

/**
 * 参数配置 数据层
 * 
 * @author gaohc
 */
public interface SysConfigMapper extends BaseMapper<SysConfig>
{

}
