package com.gaohc.system.sysuser.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gaohc.system.sysuser.domain.SysRoleMenu;
import com.gaohc.system.sysuser.mapper.SysRoleMenuMapper;
import com.gaohc.system.sysuser.ISysRoleMenuService;
import org.springframework.stereotype.Service;

/**
 * Package:com.gaohc.system.service.impl
 * Description:
 *
 * @date:2021/12/16 17:29
 * @author:gaohuichao
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {
    @Override
    public boolean checkMenuExistRole(Long menuId) {
        return lambdaQuery().eq(SysRoleMenu::getMenuId, menuId).exists();
    }
}
