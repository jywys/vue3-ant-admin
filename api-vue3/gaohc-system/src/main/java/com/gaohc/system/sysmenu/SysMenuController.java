package com.gaohc.system.sysmenu;


import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.stp.StpUtil;
import com.gaohc.common.annotation.Log;
import com.gaohc.common.constant.UserConstants;
import com.gaohc.common.core.domain.AjaxJson;
import com.gaohc.common.core.domain.entity.SysMenu;
import com.gaohc.common.enums.BusinessType;
import com.gaohc.common.utils.StringUtils;
import com.gaohc.system.sysmenu.ISysMenuService;
import com.gaohc.system.sysuser.ISysRoleMenuService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 菜单信息
 * 
 * @author ruoyi
 */
@SaCheckLogin
@RestController
@RequestMapping("/system/menu")
public class SysMenuController
{
    @Resource
    private ISysMenuService menuService;

    @Resource
    private ISysRoleMenuService sysRoleMenuService;

    /**
     * 获取菜单列表
     */
    @SaCheckPermission("system:menu:list")
    @GetMapping("/list")
    public AjaxJson list()
    {
        Long loginId = Long.parseLong(StpUtil.getLoginId().toString());
        List<SysMenu> menus = menuService.selectMenuList(loginId);
        return AjaxJson.getSuccessData(menus);
    }

    /**
     * 根据菜单编号获取详细信息
     */
    @SaCheckPermission("system:menu:query")
    @GetMapping(value = "/{menuId}")
    public AjaxJson getInfo(@PathVariable Long menuId)
    {
        return AjaxJson.getSuccessData(menuService.getById(menuId));
    }

    /**
     * 获取菜单下拉树列表
     */
    @GetMapping("/treeselect")
    public AjaxJson treeselect()
    {
        long userId = Long.parseLong(StpUtil.getLoginId().toString());
        List<SysMenu> menus = menuService.selectMenuList(userId);
        return AjaxJson.getSuccessData(menuService.buildMenuTreeSelect(menus));
    }

    /**
     * 加载对应角色菜单列表树
     */
    @GetMapping(value = "/roleMenuTreeselect/{roleId}")
    public AjaxJson roleMenuTreeselect(@PathVariable("roleId") Long roleId)
    {
        long userId = Long.parseLong(StpUtil.getLoginId().toString());
        List<SysMenu> menus = menuService.selectMenuList(userId);
        Map<String, Object> ajax = new HashMap<>();
        ajax.put("checkedKeys", menuService.selectMenuListByRoleId(roleId));
        ajax.put("menus", menuService.buildMenuTreeSelect(menus));
        return AjaxJson.getSuccessData(ajax);
    }

    /**
     * 新增菜单
     */
    @SaCheckPermission("system:menu:add")
    @Log(title = "菜单管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxJson add(@Validated @RequestBody SysMenu menu)
    {
        if (UserConstants.NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu)))
        {
            return AjaxJson.getError("新增菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        }
        else if (UserConstants.YES_FRAME.equals(menu.getIsFrame()) && !StringUtils.ishttp(menu.getPath()))
        {
            return AjaxJson.getError("新增菜单'" + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
        }
        menuService.save(menu);
        return AjaxJson.getSuccess();
    }

    /**
     * 修改菜单
     */
    @SaCheckPermission("system:menu:edit")
    @Log(title = "菜单管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxJson edit(@Validated @RequestBody SysMenu menu)
    {
        if (UserConstants.NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu)))
        {
            return AjaxJson.getError("修改菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        }
        else if (UserConstants.YES_FRAME.equals(menu.getIsFrame()) && !StringUtils.ishttp(menu.getPath()))
        {
            return AjaxJson.getError("修改菜单'" + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
        }
        else if (menu.getMenuId().equals(menu.getParentId()))
        {
            return AjaxJson.getError("修改菜单'" + menu.getMenuName() + "'失败，上级菜单不能选择自己");
        }
        menuService.updateById(menu);
        return AjaxJson.getSuccess();
    }

    /**
     * 删除菜单
     */
    @SaCheckPermission("system:menu:remove")
    @Log(title = "菜单管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{menuId}")
    public AjaxJson remove(@PathVariable("menuId") Long menuId)
    {
        if (menuService.hasChildByMenuId(menuId))
        {
            return AjaxJson.getError("存在子菜单,不允许删除");
        }
        if (sysRoleMenuService.checkMenuExistRole(menuId))
        {
            return AjaxJson.getError("菜单已分配,不允许删除");
        }
        menuService.removeById(menuId);
        return AjaxJson.getSuccess();
    }
}