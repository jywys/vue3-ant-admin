package com.gaohc.system.sysuser;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gaohc.common.core.domain.entity.SysRole;

import java.util.List;
import java.util.Set;

/**
 * 角色业务层
 * 
 * @author ruoyi
 */
public interface ISysRoleService extends IService<SysRole>
{
    /**
     * 根据条件分页查询角色数据
     * 
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    public List<SysRole> selectRoleList(SysRole role);

    /**
     * 根据用户ID查询角色权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    public Set<String> selectRolePermissionByUserId(Long userId);


    List<SysRole> selectRoleAll();

    List<Long> selectRoleListByUserId(Long userId);

    List<SysRole> selectRolesByUserId(Long userId);

    String checkRoleNameUnique(SysRole role);

    String checkRoleKeyUnique(SysRole role);

    boolean updateRoleById(SysRole role, Long userId);
}
