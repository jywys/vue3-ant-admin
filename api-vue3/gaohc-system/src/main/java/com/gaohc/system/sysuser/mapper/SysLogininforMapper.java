package com.gaohc.system.sysuser.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gaohc.system.sysuser.domain.SysLogininfor;

public interface SysLogininforMapper extends BaseMapper<SysLogininfor> {
}
