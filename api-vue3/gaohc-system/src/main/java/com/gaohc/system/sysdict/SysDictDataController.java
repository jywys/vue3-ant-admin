package com.gaohc.system.sysdict;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gaohc.common.annotation.Log;
import com.gaohc.common.core.domain.AjaxJson;
import com.gaohc.common.core.domain.entity.SysDictData;
import com.gaohc.common.enums.BusinessType;
import com.gaohc.common.utils.StringUtils;
import com.gaohc.common.utils.poi.ExcelUtil;
import com.gaohc.system.sysdict.ISysDictDataService;
import com.gaohc.system.sysdict.ISysDictTypeService;
import com.gaohc.system.sysuser.ISysUserService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * 数据字典信息
 * 
 * @author ruoyi
 */
@SaCheckLogin
@RestController
@RequestMapping("/system/dict/data")
public class SysDictDataController
{
    @Resource
    private ISysDictDataService dictDataService;

    @Resource
    private ISysDictTypeService dictTypeService;

    @Resource
    private ISysUserService sysUserService;

    @SaCheckPermission("system:dict:list")
    @GetMapping("/list")
    public AjaxJson list(SysDictData dictData) {
        Page<SysDictData> page = dictDataService.selectDictDataList(dictData);
        return AjaxJson.getSuccessData(page);
    }

    @Log(title = "字典数据", businessType = BusinessType.EXPORT)
    @SaCheckPermission("system:dict:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysDictData dictData)
    {
        List<SysDictData> list = dictDataService.list();
        ExcelUtil<SysDictData> util = new ExcelUtil<SysDictData>(SysDictData.class);
        util.exportExcel(response, list, "字典数据");
    }

    /**
     * 查询字典数据详细
     */
    @SaCheckPermission("system:dict:query")
    @GetMapping(value = "/{dictCode}")
    public AjaxJson getInfo(@PathVariable Long dictCode)
    {
        return AjaxJson.getSuccessData(dictDataService.selectDictDataById(dictCode));
    }

    /**
     * 根据字典类型查询字典数据信息
     */
    @GetMapping(value = "/type/{dictType}")
    public AjaxJson dictType(@PathVariable String dictType)
    {
        List<SysDictData> data = dictTypeService.selectDictDataByType(dictType);
        if (StringUtils.isNull(data))
        {
            data = new ArrayList<>();
        }
        return AjaxJson.getSuccessData(data);
    }

    /**
     * 新增字典类型
     */
    @SaCheckPermission("system:dict:add")
    @Log(title = "字典数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxJson add(@Validated @RequestBody SysDictData dict) {
        dictDataService.insertDictData(dict);
        return AjaxJson.getSuccess();
    }

    /**
     * 修改保存字典类型
     */
    @SaCheckPermission("system:dict:edit")
    @Log(title = "字典数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxJson edit(@Validated @RequestBody SysDictData dict) {
        dictDataService.updateDictData(dict);
        return AjaxJson.getSuccess();
    }

    /**
     * 删除字典类型
     */
    @SaCheckPermission("system:dict:remove")
    @Log(title = "字典类型", businessType = BusinessType.DELETE)
    @DeleteMapping("/{dictCodes}")
    public AjaxJson remove(@PathVariable Long[] dictCodes) {
        dictDataService.deleteDictDataByIds(dictCodes);
        return AjaxJson.getSuccess();
    }
}
