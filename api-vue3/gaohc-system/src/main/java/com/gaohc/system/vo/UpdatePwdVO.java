package com.gaohc.system.vo;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Package:com.gaohc.system.domain.vo
 * Description:
 *
 * @date:2022/2/18 14:45
 * @author:gaohuichao
 */
@Data
public class UpdatePwdVO {
    @NotBlank(message = "旧密码不能为空")
    private String oldPassword;
    @NotBlank(message = "新密码不能为空")
    private String newPassword;
}
