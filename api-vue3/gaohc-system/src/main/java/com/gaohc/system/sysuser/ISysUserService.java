package com.gaohc.system.sysuser;


import com.baomidou.mybatisplus.extension.service.IService;
import com.gaohc.common.core.domain.entity.SysUser;

/**
 * 用户 业务层
 * 
 * @author ruoyi
 */
public interface ISysUserService  extends IService<SysUser> {
    /**
     * 修改用户基本信息
     * 
     * @param user 用户信息
     */
    void updateUserProfile(SysUser user);

    SysUser getUserInfoById(Long id);

    void checkUserAllowed(SysUser user);

    void insertUserAuth(Long userId, Long[] roleIds);

    String checkUserNameUnique(String userName);

    String checkPhoneUnique(SysUser user);

    String checkEmailUnique(SysUser user);

    void insertUser(SysUser user);

    boolean updateUser(SysUser user);

    Object selectUserRoleGroup(Long userId);

    void clearRedisUser(Long userId);
}
