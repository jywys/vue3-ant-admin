package com.gaohc.system.sysuser.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gaohc.common.core.domain.entity.SysRole;
import com.gaohc.system.sysuser.domain.SysUserRole;
import com.gaohc.system.sysuser.mapper.SysRoleMapper;
import com.gaohc.system.sysuser.mapper.SysUserRoleMapper;
import com.gaohc.system.sysuser.ISysUserRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Package:com.gaohc.system.service.impl
 * Description:
 *
 * @date:2021/12/16 17:15
 * @author:gaohuichao
 */
@Service
public class SysUserRoleImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

    @Resource
    private SysRoleMapper roleMapper;

    @Override
    public List<SysRole> selectRolesByUserId(Long userId) {
        return roleMapper.selectRolesByUserId(userId);
    }
}
