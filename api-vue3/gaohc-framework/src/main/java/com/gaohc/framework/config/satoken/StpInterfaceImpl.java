package com.gaohc.framework.config.satoken;

import cn.dev33.satoken.stp.StpInterface;
import cn.dev33.satoken.stp.StpUtil;
import com.gaohc.framework.web.service.SysPermissionService;
import com.gaohc.wx.config.StpUserUtil;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 自定义权限验证接口扩展 
 */
@Component	// 打开此注解，保证此类被springboot扫描，即可完成sa-token的自定义权限验证扩展 
public class StpInterfaceImpl implements StpInterface {

	@Resource
	private SysPermissionService permissionService;

	/**
	 * 返回一个账号所拥有的权限码集合 
	 */
	@Override
	public List<String> getPermissionList(Object loginId, String loginType) {
		if (loginType.equals(StpUtil.TYPE)) {
			// 后端用户
			Long userId = Long.parseLong(StpUtil.getLoginId().toString());
			// 权限集合
			Set<String> permissions = permissionService.getMenuPermission(userId);
			return new ArrayList<>(permissions);
		} else if (loginType.equals(StpUserUtil.TYPE)) {
			// wx用户
			return new ArrayList<>();
		} else {
			return new ArrayList<>();
		}

	}

	/**
	 * 返回一个账号所拥有的角色标识集合 
	 */
	@Override
	public List<String> getRoleList(Object loginId, String loginType) {

		if (loginType.equals(StpUtil.TYPE)) {
			// 后端用户
			// 角色集合
			Long userId = Long.parseLong(StpUtil.getLoginId().toString());
			Set<String> roles = permissionService.getRolePermission(userId);
			return new ArrayList<>(roles);
		} else if (loginType.equals(StpUserUtil.TYPE)) {
			// wx用户
			return new ArrayList<>();
		} else {
			return new ArrayList<>();
		}
	}

}
