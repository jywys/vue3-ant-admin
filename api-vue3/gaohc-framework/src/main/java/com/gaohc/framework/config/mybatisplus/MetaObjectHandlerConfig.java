package com.gaohc.framework.config.mybatisplus;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.gaohc.common.constant.Constants;
import com.gaohc.common.core.domain.entity.SysUser;
import com.gaohc.common.core.redis.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;


/**
 * mybatis plus 默认值配置
 *
 * @author wenbin
 * @version V1.0
 * @date 2020年3月18日
 */
@Slf4j
@Component
public class MetaObjectHandlerConfig implements MetaObjectHandler {

    @Resource
    private RedisUtil redisUtil;

    private String getUsername (Long id) {
        String userKey = Constants.LOGIN_TOKEN_KEY + id;
        if (redisUtil.hasKey(userKey)) {
            return ((SysUser)redisUtil.get(userKey)).getUserName();
        }
        return "";
    }

    @Override
    public void insertFill(MetaObject metaObject) {
        Date currentDate = new Date();
        String[] setterNames = metaObject.getSetterNames();
        HashSet<String> setterNameSet = new HashSet<>(Arrays.asList(setterNames));
        if (setterNameSet.contains("deleted")) {
            //默认未删除
            setFieldValByName("deleted", 1, metaObject);
        }
        if (setterNameSet.contains("delFlag")) {
            //默认未删除
            setFieldValByName("delFlag", 0, metaObject);
        }
        if (setterNameSet.contains("createTime")) {
            //创建时间默认当前时间
            setFieldValByName("createTime", currentDate, metaObject);
        }
        if (setterNameSet.contains("createDate")) {
            //创建时间默认当前时间
            setFieldValByName("createDate", currentDate, metaObject);
        }
        if (setterNameSet.contains("createBy") && StpUtil.isLogin()) {
                Long userId = Long.parseLong(StpUtil.getLoginId().toString());
                //创建时间默认当前时间
                setFieldValByName("createBy", getUsername(userId), metaObject);
        }
        if (setterNameSet.contains("updateBy") && StpUtil.isLogin()) {
                Long userId = Long.parseLong(StpUtil.getLoginId().toString());
                //创建时间默认当前时间
                setFieldValByName("updateBy", getUsername(userId), metaObject);
        }
        if (setterNameSet.contains("updateTime")) {
            //创建时间默认当前时间
            setFieldValByName("updateTime", currentDate, metaObject);
        }
        if (setterNameSet.contains("updateDate")) {
            //创建时间默认当前时间
            setFieldValByName("updateDate", currentDate, metaObject);
        }


    }

    @Override
    public void updateFill(MetaObject metaObject) {
        String[] setterNames = metaObject.getSetterNames();
        HashSet<String> setterNameSet = new HashSet<>(Arrays.asList(setterNames));
        if (setterNameSet.contains("updateTime")) {
            //创建时间默认当前时间
            setFieldValByName("updateTime", new Date(), metaObject);
        }
        if (setterNameSet.contains("updateDate")) {
            //创建时间默认当前时间
            setFieldValByName("updateDate", new Date(), metaObject);
        }
        if (setterNameSet.contains("updateBy") && StpUtil.isLogin()) {
            Long userId = Long.parseLong(StpUtil.getLoginId().toString());
            //创建时间默认当前时间
            setFieldValByName("updateBy", getUsername(userId), metaObject);
        }
    }
}