package com.gaohc.framework.config.encrypt;

import com.gaohc.common.utils.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

/**
 * 自定义 DispatcherServlet 来分派 GbxHttpServletRequestWrapper
 *
 * @author gbx
 * @date 2021-08-30 14:25
 */
public class GbxDispatcherServlet extends DispatcherServlet {

    public static final List<String> methods = Arrays.asList("POST", "PUT");
    public static final List<String> noCheckUrl = Arrays.asList(
            "/wx/checkSignature", ""
    );

    /**
     * 加密功能服务接口类
     */

    public GbxDispatcherServlet() {
    }

    /**
     * 包装自定义的 request
     *
     * @param request
     * @param response
     * @throws Exception
     */
    @Override
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String requestURI = request.getRequestURI();
        // 对符合要求的指定的请求路径接口进行解密操作
        if (StringUtils.isNotBlank(requestURI) && methods.contains(request.getMethod()) && !noCheckUrl.contains(requestURI)) {
            super.doDispatch(new DataEncryptionWrapper(request), response);
        } else {
            super.doDispatch(new HttpServletRequestWrapper(request), response);
        }

    }
}