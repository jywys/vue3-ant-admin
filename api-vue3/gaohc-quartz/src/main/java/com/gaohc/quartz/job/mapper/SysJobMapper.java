package com.gaohc.quartz.job.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gaohc.quartz.job.domain.SysJob;

/**
 * 调度任务信息 数据层
 * 
 * @author ruoyi
 */
public interface SysJobMapper extends BaseMapper<SysJob>
{

}
