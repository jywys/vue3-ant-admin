package com.gaohc.quartz.job;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gaohc.common.annotation.Log;
import com.gaohc.common.constant.Constants;
import com.gaohc.common.core.domain.AjaxJson;
import com.gaohc.common.enums.BusinessType;
import com.gaohc.common.utils.StringUtils;
import com.gaohc.common.utils.poi.ExcelUtil;
import com.gaohc.quartz.job.domain.SysJob;
import com.gaohc.quartz.util.CronUtils;
import org.quartz.SchedulerException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 调度任务信息操作处理
 * 
 * @author ruoyi
 */
@RestController
@RequestMapping("/monitor/job")
public class SysJobController
{
    @Resource
    private ISysJobService jobService;

    /**
     * 查询定时任务列表
     */
    @SaCheckPermission("monitor:job:list")
    @GetMapping("/list")
    public AjaxJson list(SysJob sysJob)
    {
        Page<SysJob> page = new Page<>(1, 10);
        jobService.page(page);
        return AjaxJson.getSuccessData(page);
    }

    /**
     * 导出定时任务列表
     */
    @SaCheckPermission("monitor:job:export")
    @Log(title = "定时任务", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysJob sysJob)
    {
        List<SysJob> list = jobService.selectJobList(sysJob);
        ExcelUtil<SysJob> util = new ExcelUtil<SysJob>(SysJob.class);
        util.exportExcel(response, list, "定时任务");
    }

    /**
     * 获取定时任务详细信息
     */
    @SaCheckPermission("monitor:job:query")
    @GetMapping(value = "/{jobId}")
    public AjaxJson getInfo(@PathVariable("jobId") Long jobId)
    {
        return AjaxJson.getSuccessData(jobService.getById(jobId));
    }

    /**
     * 新增定时任务
     */
    @SaCheckPermission("monitor:job:add")
    @Log(title = "定时任务", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxJson add(@RequestBody SysJob job) throws SchedulerException
    {
        if (!CronUtils.isValid(job.getCronExpression()))
        {
            return AjaxJson.getError("新增任务'" + job.getJobName() + "'失败，Cron表达式不正确");
        }
        else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.LOOKUP_RMI))
        {
            return AjaxJson.getError("新增任务'" + job.getJobName() + "'失败，目标字符串不允许'rmi://'调用");
        }
        else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.LOOKUP_LDAP))
        {
            return AjaxJson.getError("新增任务'" + job.getJobName() + "'失败，目标字符串不允许'ldap://'调用");
        }
        else if (StringUtils.containsAnyIgnoreCase(job.getInvokeTarget(), new String[] { Constants.HTTP, Constants.HTTPS }))
        {
            return AjaxJson.getError("新增任务'" + job.getJobName() + "'失败，目标字符串不允许'http(s)//'调用");
        }
        else if (StringUtils.containsAnyIgnoreCase(job.getInvokeTarget(), Constants.JOB_ERROR_STR))
        {
            return AjaxJson.getError("新增任务'" + job.getJobName() + "'失败，目标字符串存在违规");
        }
        return AjaxJson.getSuccessData(jobService.insertJob(job));
    }

    /**
     * 修改定时任务
     */
    @SaCheckPermission("monitor:job:edit")
    @Log(title = "定时任务", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxJson edit(@RequestBody SysJob job) throws SchedulerException
    {
        if (!CronUtils.isValid(job.getCronExpression()))
        {
            return AjaxJson.getError("修改任务'" + job.getJobName() + "'失败，Cron表达式不正确");
        }
        else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.LOOKUP_RMI))
        {
            return AjaxJson.getError("修改任务'" + job.getJobName() + "'失败，目标字符串不允许'rmi://'调用");
        }
        else if (StringUtils.containsIgnoreCase(job.getInvokeTarget(), Constants.LOOKUP_LDAP))
        {
            return AjaxJson.getError("修改任务'" + job.getJobName() + "'失败，目标字符串不允许'ldap://'调用");
        }
        else if (StringUtils.containsAnyIgnoreCase(job.getInvokeTarget(), new String[] { Constants.HTTP, Constants.HTTPS }))
        {
            return AjaxJson.getError("修改任务'" + job.getJobName() + "'失败，目标字符串不允许'http(s)//'调用");
        }
        else if (StringUtils.containsAnyIgnoreCase(job.getInvokeTarget(), Constants.JOB_ERROR_STR))
        {
            return AjaxJson.getError("修改任务'" + job.getJobName() + "'失败，目标字符串存在违规");
        }
        return AjaxJson.getSuccessData(jobService.updateJob(job));
    }

    /**
     * 定时任务状态修改
     */
    @SaCheckPermission("monitor:job:changeStatus")
    @Log(title = "定时任务", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxJson changeStatus(@RequestBody SysJob job) throws SchedulerException
    {
        SysJob newJob = jobService.getById(job.getJobId());
        newJob.setStatus(job.getStatus());
        return AjaxJson.getSuccessData(jobService.changeStatus(newJob));
    }

    /**
     * 定时任务立即执行一次
     */
    @SaCheckPermission("monitor:job:changeStatus")
    @Log(title = "定时任务", businessType = BusinessType.UPDATE)
    @PutMapping("/run")
    public AjaxJson run(@RequestBody SysJob job) throws SchedulerException
    {
        jobService.run(job);
        return AjaxJson.getSuccess();
    }

    /**
     * 删除定时任务
     */
    @SaCheckPermission("monitor:job:remove")
    @Log(title = "定时任务", businessType = BusinessType.DELETE)
    @DeleteMapping("/{jobIds}")
    public AjaxJson remove(@PathVariable Long[] jobIds) throws SchedulerException
    {
        jobService.deleteJobByIds(jobIds);
        return AjaxJson.getSuccess();
    }
}
