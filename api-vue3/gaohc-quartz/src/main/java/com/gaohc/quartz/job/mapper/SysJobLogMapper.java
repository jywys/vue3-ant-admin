package com.gaohc.quartz.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gaohc.quartz.job.domain.SysJobLog;

/**
 * 调度任务日志信息 数据层
 * 
 * @author ruoyi
 */
public interface SysJobLogMapper extends BaseMapper<SysJobLog>
{

}
