package com.gaohc.controller.monitor;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gaohc.common.annotation.Log;
import com.gaohc.common.constant.Constants;
import com.gaohc.common.core.domain.AjaxJson;
import com.gaohc.common.core.domain.entity.SysUser;
import com.gaohc.common.core.redis.RedisUtil;
import com.gaohc.common.enums.BusinessType;
import com.gaohc.common.utils.StringUtils;
import com.gaohc.system.sysuser.SysUserOnline;
import com.gaohc.system.sysuser.ISysUserOnlineService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * 在线用户监控
 * 
 * @author ruoyi
 */
@SaCheckLogin
@RestController
@RequestMapping("/monitor/online")
public class SysUserOnlineController
{
    @Resource
    private ISysUserOnlineService userOnlineService;

    @Resource
    private RedisUtil redisUtil;

    @SaCheckPermission("monitor:online:list")
    @GetMapping("/list")
    public AjaxJson list(String ipaddr, String userName)
    {
        Collection<String> keys = redisUtil.keys(Constants.LOGIN_TOKEN_KEY + "*");
        List<SysUserOnline> userOnlineList = new ArrayList<>();
        for (String key : keys)
        {
            SysUser user = (SysUser)redisUtil.get(key);
            if (StringUtils.isNotEmpty(ipaddr) && StringUtils.isNotEmpty(userName))
            {
                if (StringUtils.equals(ipaddr, user.getLoginIp()) && StringUtils.equals(userName, user.getUserName()))
                {
                    userOnlineList.add(userOnlineService.selectOnlineByInfo(ipaddr, userName, user));
                }
            }
            else if (StringUtils.isNotEmpty(ipaddr))
            {
                if (StringUtils.equals(ipaddr, user.getLoginIp()))
                {
                    userOnlineList.add(userOnlineService.selectOnlineByIpaddr(ipaddr, user));
                }
            }
            else if (StringUtils.isNotEmpty(userName))
            {
                if (StringUtils.equals(userName, user.getUserName()))
                {
                    userOnlineList.add(userOnlineService.selectOnlineByUserName(userName, user));
                }
            }
            else
            {
                userOnlineList.add(userOnlineService.loginUserToUserOnline(user));
            }
        }
        Collections.reverse(userOnlineList);
        userOnlineList.removeAll(Collections.singleton(null));
        Page<SysUserOnline> page = new Page<>();
        page.setRecords(userOnlineList);
        return AjaxJson.getSuccessData(page);
    }

    /**
     * 强退用户
     */
    @SaCheckPermission("monitor:online:forceLogout")
    @Log(title = "在线用户", businessType = BusinessType.FORCE)
    @DeleteMapping("/{tokenId}")
    public AjaxJson forceLogout(@PathVariable String tokenId)
    {
        redisUtil.del(Constants.LOGIN_TOKEN_KEY + tokenId);
        return AjaxJson.getSuccess();
    }
}
