package com.gaohc.controller.common;

import com.gaohc.common.config.SystemConfig;
import com.gaohc.common.constant.Constants;
import com.gaohc.common.core.domain.AjaxJson;
import com.gaohc.common.core.redis.RedisUtil;
import com.gaohc.common.utils.sign.Base64;
import com.gaohc.common.utils.uuil.IdUtils;
import com.gaohc.system.sysconfig.ISysConfigService;
import com.google.code.kaptcha.Producer;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 验证码操作处理
 * 
 * @author gaohc
 */
@RestController
public class CaptchaController
{
    @Resource(name = "captchaProducer")
    private Producer captchaProducer;

    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Resource
    private RedisUtil redisUtil;
    
    @Resource
    private ISysConfigService configService;
    /**
     * 生成验证码
     */
    @GetMapping("/captchaImage")
    public AjaxJson getCode(HttpServletResponse response) {

        boolean captchaOnOff = configService.selectCaptchaOnOff();
        Map<String, Object> map = new HashMap<>();
        map.put("captchaOnOff", captchaOnOff);
        if (!captchaOnOff)
        {
            return AjaxJson.getSuccessData(map);
        }

        // 保存验证码信息
        String uuid = IdUtils.simpleUUID();
        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;

        String capStr = null, code = null;
        BufferedImage image = null;

        // 生成验证码
        String captchaType = SystemConfig.getCaptchaType();
        if ("math".equals(captchaType))
        {
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            image = captchaProducerMath.createImage(capStr);
        }
        else if ("char".equals(captchaType))
        {
            capStr = code = captchaProducer.createText();
            image = captchaProducerMath.createImage(capStr);
        }

        redisUtil.set(verifyKey, code, Constants.CAPTCHA_EXPIRATION * 60);
        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try
        {
            ImageIO.write(image, "jpg", os);
        }
        catch (IOException e)
        {
            return AjaxJson.getError(e.getMessage());
        }

        map.put("uuid", uuid);
        map.put("img", Base64.encode(os.toByteArray()));
        return AjaxJson.getSuccessData(map);
    }
}
