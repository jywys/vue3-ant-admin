package com.gaohc.controller.monitor;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import com.gaohc.common.core.domain.AjaxJson;
import com.gaohc.framework.web.domain.Server;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控
 * 
 * @author ruoyi
 */
@SaCheckLogin
@RestController
@RequestMapping("/monitor/server")
public class ServerController
{
    @SaCheckPermission("monitor:server:list")
    @GetMapping()
    public AjaxJson getInfo() throws Exception
    {
        Server server = new Server();
        server.copyTo();
        return AjaxJson.getSuccessData(server);
    }
}
