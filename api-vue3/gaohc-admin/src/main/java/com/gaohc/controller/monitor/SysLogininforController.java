package com.gaohc.controller.monitor;

import cn.dev33.satoken.annotation.SaCheckLogin;
import cn.dev33.satoken.annotation.SaCheckPermission;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.gaohc.common.annotation.Log;
import com.gaohc.common.core.domain.AjaxJson;
import com.gaohc.common.enums.BusinessType;
import com.gaohc.common.utils.poi.ExcelUtil;
import com.gaohc.system.sysuser.domain.SysLogininfor;
import com.gaohc.system.sysuser.ISysLogininforService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 系统访问记录
 * 
 * @author ruoyi
 */
@SaCheckLogin
@RestController
@RequestMapping("/monitor/logininfor")
public class SysLogininforController
{
    @Resource
    private ISysLogininforService logininforService;

    @SaCheckPermission("monitor:logininfor:list")
    @GetMapping("/list")
    public AjaxJson list(SysLogininfor logininfor)
    {
        Page<SysLogininfor> page = new Page<>(1, 10);
        logininforService.lambdaQuery().page(page);
        return AjaxJson.getSuccessData(page);
    }

    @Log(title = "登录日志", businessType = BusinessType.EXPORT)
    @SaCheckPermission("monitor:logininfor:export")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysLogininfor logininfor)
    {
        List<SysLogininfor> list = logininforService.selectLogininforList(logininfor);
        ExcelUtil<SysLogininfor> util = new ExcelUtil<>(SysLogininfor.class);
        util.exportExcel(response, list, "登录日志");
    }

    @SaCheckPermission("monitor:logininfor:remove")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{infoIds}")
    public AjaxJson remove(@PathVariable Long[] infoIds)
    {
        logininforService.deleteLogininforByIds(infoIds);
        return AjaxJson.getSuccess();
    }

    @SaCheckPermission("monitor:logininfor:remove")
    @Log(title = "登录日志", businessType = BusinessType.CLEAN)
    @DeleteMapping("/clean")
    public AjaxJson clean()
    {
        logininforService.cleanLogininfor();
        return AjaxJson.getSuccess();
    }
}
