package com.gaohc;

import com.baomidou.mybatisplus.core.toolkit.AES;
class ApiVue3ApplicationTests {

    void contextLoads() {
    }

    public static void main(String[] args) {
        // mybatis-plus 生成加密数据库链接密文
        // 生成 16 位随机 AES 密钥
        String randomKey = AES.generateRandomKey();

        // 随机密钥加密
//        String result = AES.encrypt("jdbc:mysql://127.0.0.1:3306/vue-admin?useUnicode=true&characterEncoding=utf8&useSSL=false", randomKey);
//        String result = AES.encrypt("jdbc:mysql://127.0.0.1:3306/vue-admin?useUnicode=true&characterEncoding=utf8&useSSL=false", "825488e41d406f08");
        String result = AES.encrypt("root", "825488e41d406f08");
        System.out.println(randomKey + "---------" + result);
    }

}
