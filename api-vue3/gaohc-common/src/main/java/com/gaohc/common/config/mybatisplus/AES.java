package com.gaohc.common.config.mybatisplus;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class AES {

    // 密钥
    public static String key = "AD42F6697B035B7580E4FEF93BE20BAD";
    // 偏移量
    private static final int offset = 16;
    private static final String transformation = "AES/CBC/PKCS5Padding";
    private static final String algorithm = "AES";

    /**
     * 加密
     */
    public static String encrypt(String content) {
        return encrypt(content, key);
    }

    /**
     * 解密
     */
    public static String decrypt(String content) {
        return decrypt(content, key);
    }

    /**
     * 加密
     *
     * @param content 需要加密的内容
     * @param key     加密密码
     */
    public static String encrypt(String content, String key) {
        try {
            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), algorithm);
            IvParameterSpec iv = new IvParameterSpec(key.getBytes(), 0, offset);
            Cipher cipher = Cipher.getInstance(transformation);
            String charset = "utf-8";
            byte[] byteContent = content.getBytes(charset);
            cipher.init(Cipher.ENCRYPT_MODE, skey, iv);// 初始化
            byte[] result = cipher.doFinal(byteContent);
            return Base64.getEncoder().encodeToString(result); // 加密
        } catch (Exception e) {
            // LogUtil.exception(e);
        }
        return null;
    }

    /**
     * AES（256）解密
     * @param content 待解密内容
     * @param key     解密密钥
     * @return 解密之后
     */
    public static String decrypt(String content, String key) {
        try {

            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), algorithm);
            IvParameterSpec iv = new IvParameterSpec(key.getBytes(), 0, offset);
            Cipher cipher = Cipher.getInstance(transformation);
            cipher.init(Cipher.DECRYPT_MODE, skey, iv);// 初始化
            byte[] result = cipher.doFinal(Base64.getDecoder().decode(content));
            return new String(result); // 解密
        } catch (Exception e) {
            //LogUtil.exception(e);
        }
        return null;
    }

    public static void main(String[] args) {
        String e10adc3949ba59abbe56e057f20f883e = decrypt("aQi0lCtlebjm2ArFTJlWvPlKBAmm3qGW4+515Ne9iEqX5d/Bde/c2mLfqERbw+W6");
        System.out.println(e10adc3949ba59abbe56e057f20f883e);
    }

}
