package com.gaohc.common.core.controller;

import cn.dev33.satoken.stp.StpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cglib.core.EmitUtils;

/**
 * web层通用数据处理
 * 
 * @author ruoyi
 */
@Slf4j
public class BaseController {

    public Long getUserId(){
        Object loginId = StpUtil.getLoginId();
        if (loginId == null) {
            return null;
        }
        return Long.parseLong(loginId+"");
    }

}
