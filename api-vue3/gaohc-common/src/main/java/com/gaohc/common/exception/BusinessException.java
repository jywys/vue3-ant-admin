package com.gaohc.common.exception;

import com.gaohc.common.core.domain.AjaxJson;

public class BusinessException extends RuntimeException{
    private final int code;
    private final String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return this.msg;
    }
    public BusinessException(String msg) {
        super(msg);
        this.msg = msg;
        this.code = AjaxJson.CODE_ERROR;
    }

    public BusinessException(int code, String msg) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }
}
