import request from '@/utils/request.js'

export function wxlogin(code) {
	return request.get("/wx/user/login", { code: code })
	
}

